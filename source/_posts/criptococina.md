---
title: criptococina
date: 2018-07-31 17:02:08
tags:
  - faircoop
  - criptomonedas
  - cocina
photos:
- /images/criptococina.jpg
---

Una de las ventajas de tener un lugar _nuestro_, es la posibilidad de hacer
pequeños experimentos, probar nuestras propias ideas con respecto a las formas
de relacionarnos.

Desde hace un tiempo ya veníamos escuchando hablar de criptomonedas, en
particular nos interesó mucho la propuesta de FairCoin, pero una moneda no
sirve si no la acepta nadie y en este momento, en dónde estamos, las
criptomonedas están en pañales.

Sin embargo encontramos una implementación mínima para solucionar un problema
puntual. La idea fue reemplazar "el tarrito" de la cocina, que era un pequeño
frasco plástico que estaba en la cocina donde dejábamos plata para las compras.
Entonces por ejemplo, cada vez que se acababa el aceite uno sacaba plata del
tarrito para reponerlo y cada vez que almorzaba hacía un pequeño aporte al
tarrito.

Ahora el funcionamiento es similar, pero sin pesos, cada vez que almorzamos
hacemos un _pago_ de 1 FAIR y cada vez que alguien compra algo se cobra el
costo en FAIR. La criptococina es una billetera de FairCoin.

Esto tiene muchas ventajas, por un lado, como el FairCoin esta valuado en euros
los precios casi no varían, 1 FAIR es un almuerzo, como utilizamos una
billetera multi-firma para la criptococina nadie puede vaciar el tarron sin que
dos personas estén de acuerdo, no hay plata dando vueltas, no hay problemas con
el cambio y como las billeteras llevan un registro de los pagos nunca queda
duda de si pagaste o no.

La verdad, que desde que empezamos con este sistema, la cocina está
funcionando mucho mejor, cuando quieran venir a almorzar, ya saben, sólo cuesta
1 FAIR.
