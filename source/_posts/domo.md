---
title: Domo Geodésico
comments: true
date: 2018-07-15 10:00:00
photos:
- /images/domo.jpg
---

Este sábado nos juntamos con la gente de la
[Mutual de Voluntarios](http://mutualdevoluntarios.org.ar/) a armar un
domo geodésico. Este domo va a ser usado para que les chiques del barrio los
troncos tengan un lugar cerrado para realizar distintas actividades, como tomar
la copa de leche.  Pero también nos sirvió como experiencia para mostrar que se
puede, con pocos materiales y poco tiempo (4 horas), armar una estructura
funcional y muy resistente.

Esperamos a todes les que se quieran sumar en la segunda etapa, donde vamos a
ver como lo cubrimos.
