---
layout: blog
title: ¿Conocés los faircoins?
date: 2019-03-18T14:35:12.079Z
cover_index: /images/uploads/pienso-en-faircoins.jpg
cover_detail: ''
tags:
  - faircoop
---
Sí, nosotres también estamos en la movida de las criptomonedas. La que usamos como grupo tiene un origen muy simpático, cuando Enric Durán, el posteriormente apodado "[Robin Hood](https://www.20minutos.es/noticia/1921859/0/reaparece-robin/bancos-desde/clandestinidad/) de los Bancos", pidió un montón de créditos y los donó a diferentes organizaciones sociales, una de ellas fue el proyecto de una criptomoneda que buscara la justicia social.

Podés contactarnos para más información, o directamente ir a <https://fair.coop>. Más info [acá mismo](https://hackatory.netlify.com/2018/12/17/faircoop-santafe/), en nuestro blog.

Ahí vamos!
