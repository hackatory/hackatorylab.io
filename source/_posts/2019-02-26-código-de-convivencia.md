---
layout: blog
title: Código de convivencia
date: 2019-02-26T15:02:07.593Z
---
Creemos en crear un espacio no violento donde puedan convivir diversidad de personas e iniciativas, por lo que evitamos comportamientos fachos, machistas, racistas, misóginos, trolls o boicoteadores.

Aceptamos la adhocracia, si ves que hay que hacer algo, hacélo :). No sabemos las mismas cosas , así que proponemos el siguiente orden de ejecución: Hacerlo, Intentarlo, Preguntar, Pedir ayuda.

Si ensuciaste algo, limpiálo.

Hackatory es un espacio amigo para: la lactancia materna, la integración familiar, el aprendizaje de les niñes. Si sos parte de Hackatory, aceptás que de vez en cuando un niñe va a llorar y algún pecho se va a exponer, pero no precisamente para tu recreación.

Hackatory es un espacio para todes, con énfasis en colaborar. Si tenés niñes que van a formar parte de Hackatory sé respetuose de aquellos que necesitan trabajar, estudiar y actividades similares.

## Comidas

Sentíte libre de consumir lo que está en Hackatory: mate, galletitas, café, azúcar. Incluso lo que se necesita para cocinar al medio día. Para la administración de los gastos que involucran esta actividad inventamos algo que se llama “La cryptococina”. Básicamente es una billetera en Faircoins que es administrada por todos los integrantes, cuando alguien quiera consumir algo del espacio, puede aportar FairCoins a esta billetera. Algunos ejemplos son:



Almorzar: 1f

meriendas, café, mate, etc: 1f semanal

También podes traer cosas al espacio para que otros consuman y pedir el correspondiente valor en fairs. La crytococina no maneja dinero FIAT.
