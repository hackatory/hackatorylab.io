---
title: Asamblea diciembre FairCoop
date: 2018-12-17T15:00:00.000Z
cover_index: /images/uploads/faircoop2.png
cover_detail: /images/uploads/cryto1.jpg
tags:
  - faircoop
---

Entre las diversas actividades del hackatory, hace ya unos meses que el mismo
funciona como base de operaciones del nodo Santa Fe de [FairCoop], que es un
movimiento nacido en España, que se propone la difícil tarea de construir un
sistema económico post-capitalista.

Las bases del movimiento son la revolución integral, la ética hacker y la
organización p2p y se utiliza como bandera una criptomoneda llamada [FairCoin],
que tiene como característica especial, no requerir cantidades descomunales de
energía para funcionar.

Como nodo, nos planteamos desde un principio funcionar, lo más posible, de
forma autónoma, por lo que nuestra participación en el movimiento global es más
bien mínima y nos mantenemos un poco aislados. Lo hicimos así porque al ser un
movimiento muy fuerte en Europa, nos parece importante darnos tiempo para
construir *desde abajo* nuestra propia autonomía.

La última asamblea, llevada a cabo durante diciembre, fue parcialmente
frustrada por una fuerte lluvia, que impidió la llegada de muchos invitados a
los cuales pensábamos presentar el movimiento, sin embargo, estar los tres
miembros más activos nos permitió repensar algunas cuestiones de fondo, como la
autonomía.

Otra cuestión que se planteó fue la cuestión del riesgo del proyecto y nuestros
miedos al respecto, por ejemplo por la posibilidad de que el valor de la moneda
caiga de forma abrupta e irremediable. Ante esto, reafirmamos ciertas
decisiones, entre las cuales se cuenta introducir una cantidad controlada de
FairCoin en el sistema, aclarar que no son para ahorrar y divulgar mediante el
"boca a boca" entre personas de confianza.

De este modo, nuestro primer objetivo es aprender a utilizar criptomonedas en
los distintos contextos que se nos presentan, a organizarnos de forma
horizontal, a pensarnos como prosumidores y a contactarnos con proyectos
afines.

Otra de las decisiones que tomamos, es publicar las "actas" de las asambleas en
forma de entradas en la página del hackatory, siendo ésta la primera de ellas.

Por último, decidimos crear una criptodespensa en el mismo hackatory con
elementos no-perecederos, inicialmente mermelada, naranpol y yerba. Evaluamos
utilizar herramientas como Bazar y Kolabora, pero dada la escala que manejamos
y el tiempo disponible decidimos hacerla funcionar como autoservicio (sin nadie
atendiendo), siguiendo el modelo de la [criptococina].

  [FairCoop]: https://fair.coop/es
  [FairCoin]: https://coinmarketcap.com/currencies/faircoin/
  [criptococina]: /2018/07/31/criptococina/
