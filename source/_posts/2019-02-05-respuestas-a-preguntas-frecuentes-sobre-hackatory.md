---
layout: blog
title: Respuestas a preguntas frecuentes sobre Hackatory
date: 2019-02-09T10:51:25.461Z
cover_index: /images/uploads/question-marks-color.jpg
---
**Son una ONG?**

Nop. No por ahora, y quizás nunca, en algún momento lo plantearemos, tal vez.

**Son una empresa privada?**

Nooo, vade retro, para nada.

**¿Dan una definición de una buena vez?**

Bueno, pero es que por un lado pasa que no estamos definidos, sino en continua construcción.

De momento somos un espacio físico de colaboración, orientado a cuestiones "tecnológicas". Lo de la tecnología es algo que tampoco podríamos definir suficiente.

**Pero son hackers, ¿no? Como la **[**serie**](https://youtu.be/RbArCOuDevs?t=26)** de Carlos Calvo...**

No, para nada. Lo de hackers en Hackatory es polisémico, pero el significado principal estaría en el libro "la ética hacker", un resumen del libro sería que nos sentimos identificados con la gente que comparte en lugar de guardarse el conocimiento para sí. Entonces "hacker" para nosotres sería sinónimo de "persona que comparte".

**Pará, pará, pará… ¿estás usando lenguaje inclusivo?**

Sí :)

**Al fin una respuesta directa. ¿Ustedes son gays?**

No se tiene que pertenecer a la comunidad LGBTIQ+ para defender los derechos de las personas.

**Ok, pero ¿qué es lo que se dedican a hacer ustedes, robar dinero por computadora o ir a marchas?**

Las actividades del Hackatory son de otra índole. Primero y principal, compartimos el espacio a quien se sienta en sintonía con nosotros, con lo cual sólo nazis quedarían excluidos de una; mientras que al resto los invitamos a leer nuestro [código de convivencia](https://hackatory.netlify.com/2019/02/26/2019-02-26-c%C3%B3digo-de-convivencia/).

Hacemos diferentes actividades. Tenemos un [grupo de lectura](https://hackatory.netlify.com/2018/06/16/la-pizza-lectora-4/); un taller de aprendizaje de inglés autogestivo; somos la sede del Nodo Faircoop Santa Fe; estamos en la organización de las jornadas de género y software libre; y se van dando nuevas actividades cada tanto, en la medida que alguien se cope.

Si seguís teniendo dudas, podés venir y vernos.

**Antes de terminar, ¿cuánto me cobran por ir?**

No cobramos, en todo caso podés ver [lo que pedimos a cambio](https://hackatory.netlify.com/2018/10/03/2018-10-03-lo-que-queremos-a-cambio/).
