---
title: Mudanza
date: 2018-08-08T17:02:08.000Z
cover_index: ''
cover_detail: ''
---
chicxs, el sábado 18, a partir de las 14:00 vamos a juntarnos en Hackatory todos
lo que puedan para pintar y arreglar el departamento porque... Nos mudamos!!!
Sí señor. Hackatory se muda y tira todo por la ventana. Esto último es casi
literal porque nos movemos al departamento de al lado, pasamos del departamento
4 al 3 que es un espacio un poco más grande y luminoso.

"Me da muchísima paja trabajar un sábado", estarás pensando, pero para
convencer a los más perezosos también habrá pizzas y otros aperitivos para
disfrutar durante el día. Y si todavía te quedan dudas, habrá música en vivo y
torta fritas con mate.

Dejanos en los comentarios tu momento Hackatory del año, te esperamos!
