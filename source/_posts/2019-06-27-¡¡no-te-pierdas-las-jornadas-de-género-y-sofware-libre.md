---
layout: blog
title: ¡¡No te pierdas las Jornadas de Género y Sofware Libre!!
date: 2019-06-27T14:43:23.718Z
cover_index: /images/uploads/jornada-g-sl-con-auspiciantes.jpg
cover_detail: /images/uploads/jornada-g-sl-con-auspiciantes.jpg
photos:
  - /images/uploads/cronograma-viernes-5-jdg-sl.jpg
---
Jueves 4 y viernes de julio, en la Ciudad Universitaria de Santa Fe, un montón de charlas y talleres, sobre software libre, sobre género, y sobre los diferentes entramados de ambos mundos.
