---
title: R-Ladies
date: 2018-09-10 17:02:08
photos:
- /images/R-LadiesGlobal.png
---
El pasado 2 de Agosto, Hackatory fue sede de otra meetup de R-ladies, capítulo
Santa Fe.  Para los que no lo conozcan, R es un entorno y lenguaje de
programación con un enfoque al análisis estadístico.  Es una implementación de
software libre del lenguaje S. Se trata de uno de los lenguajes más utilizados
en investigación por la comunidad estadística, siendo además muy popular en el
campo de la minería de datos, la investigación biomédica, la bioinformática y
las matemáticas financieras.  Es parte del sistema GNU y se distribuye bajo la
licencia GNU GPL.  R-ladies tiene como principal objetivo apoyar a los
entusiastas de R que se identifican como minorías de baja representación
(mujeres y personas de géneros disidentes) para alcanzar su máximo potencial en
la programación R, al construir una red colaborativa de líderes R, mentores,
estudiantes y desarrolladores. Esto busca facilitar el progreso individual y
colectivo en el mundo.  Así que invitamos a R-ladies a ser parte de Hackatory y
compartimos unas buenas charlas (mientras disfrutábamos de unos sandwuichines).
Las disertantes fueron:

- Daniela Vázquez (economista, actualmente es Data Scientist en Idatha,
  co-fundadora de los capítulos de R-Ladies de Buenos Aires y Montevideo, y
  parte de NASA Datanauts).  Su charla: "Abriendo y Analizando los Diarios de
  Sesiones del Parlamento uruguayo con R", nos permitió aprender a nalizar
  contenido de los datos publicados del parlamento uruguayo. Web scraping para
  obtener texto de difícil acceso sistemático en la web y en formato pdf y
  exploró el "sentimiento" de las sesiones intentando averiguar de qué temas se
  habló en cada una, usando el paquete tidytext.  Un interesante repaso de su
  búsqueda para extrer el texto de los archivos desde la página web,
  analizarlos y publicar los datos en formato de gráficos, que pudo difundir
  vía Twitter :) Mención especial para el paquete "lexicon", que nos da una
  herramienta para analizar el sentimiento de un texto "como la suma de los
  sentimientos asociados a las palabras individualmente consideradas".

- Natsumi Shokida (estudiante avanzada de la carrera de Economía (FCE-UBA). Se
  dedica al análisis de datos y la realización de estudios metodológicos en el
  área de Pobreza e Ingresos de la Dirección de la Encuesta Permanente de
  Hogares (EPH-INDEC), a título personal; también es colaboradora en Economía
  Femini(s)ta y es parte de RLadies Buenos Aires) Su charla: "Indicadores de
  Desigualdad de Género Utilizando Datos Abiertos de la Encuesta Permanente de
  Hogares y Rmarkdown", nos habló de la desigualdad de género en la composición
  del mercado de trabajo, la percepción de ingresos y demás, mediante
  indicadores calculados con bases públicas de la EPH (Encuesta Permanente de
  Hogares – INDEC – Argentina).  Nos mostró cómo descargar bases desde la
  página de INDEC y luego, distintas funciones para la clasificación y
  presentación de los datos, generando gráficos que permitían visualizar la
  Composición del Mercado de Trabajo según la inserción laboral por edad y
  sexo, o si el empleo era no registrado, el acceso a cargos jerárquicos y más.
  El grupo qued+o maravillado con el formato de presentación, que permite
  automatizar el informe de los datos. Una herramienta más que útil.

- Mariel Lovatto (Actualmente cursa la Maestría en Estadística aplicada en UNR.
  Trabaja como ayudante de catedra en Probabilidad y Estadística en Facultad de
  Ingeniería Química, es JTP en Matemática Básica en la Facultad de Ciencias
  Económicas y es parte del observatorio social de la UNL.) Su charla: "R: Usos
  y Desafíos" trata sobre el proceso de aprendizaje de Mariel, en la
  programación en R a partir de su uso. Nos mostró ejemplos de aplicación, la
  descripción de los desafíos cotidianos y los procesos de superación.  Un
  paseo por el propio camino de Mariel para descubrir R. Nos esquematizó de
  manera muy didáctica cuales son aquellos desafios que debemos identificar
  para comenzar a buscar una solución a travez de R.

La jornada fue muy enriquesedora y, a pesar del frio, les asistentes se
quedaron hasta el final, compartiendo experiencias y dudas. También se sumó la
mini R-Lady, que ya es habitué de Hackatory pues disfrutó de todos los
encuentros de "La Pizza Lectora".

Muchas Gracias R-ladies por hacernos partícipes de su misión y esperamos verlas
pronto con un nuevo encuentro, esta vez en nueva sede.  Para la gente que
quiera sumarse al capítulo de R-ladies Santa Fe, puede seguirles en alguna de
sus redes sociales. A investigar!

![mini Rlady](/images/rladies2.jpg)
![Foto grupal](/images/rladies1.jpg)

###Contacto
santafe@rladies.org

[https://twitter.com/RLadiesSantaFe](https://twitter.com/RLadiesSantaFe)

[https://www.meetup.com/rladies-santa-fe/](https://www.meetup.com/rladies-santa-fe/)
