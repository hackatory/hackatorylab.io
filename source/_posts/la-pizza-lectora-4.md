---
title: la Pizza Lectora
date: 2018-06-16T10:00:00.000Z
photos:
  - /images/Pizza_Dimension.png
---
# El club de lectura de Hackatory.

A mitad del 2018 se creó el Club de Lectura en Hackatory. Fue una idea que sugerí en una reunión porque hacía mucho tiempo que quería participar en uno, pero nunca se daba la oportunidad. Me cuesta hablar sobre el Club sin hablar del Hack, así que voy a hacer un esfuerzo, porque me gustaría contar mi experiencia en Hackatory en otro momento.

Volviendo a los libros, pensé que era una idea que no iba a prosperar, ya lo había sugerido en otros ámbitos, en otro tiempo, y no había funcionado, pero esta vez la respuesta me sorprendió… a los días ya se había organizado la fecha para la primera reunión y el primer título para debatir: Un mundo feliz.

La tarde del primer encuentro se pasó volando, entre mates, Bernard, Soma, facturas y chipá. En las siguientes reuniones a veces compartimos noche de pizzas con bomberos que más bien eran Firemen. Otras veces compartimos la comida con cerdos con perros, un caballo, una cabra y un burro. Una noche nos escondimos de El Partido con Winston y Julia. Pero también tuvimos una tarde de brownies con Ralph, Jack y Piggy.

He llegado a apreciar mucho a esto que hemos formado, y más aún a la gente que participa en este espacio. Tenemos un lugar para debatir ideas y perspectivas, no sólo de los libros que leemos, sino de lo que nos rodea, y aunque tengamos pensamientos y reflexiones dispares, las exponemos siempre a partir del respeto. Éste es el intercambio de ideas, también con la cuota de empatía que -creo yo-, nos nutre como personas.

La última reunión fue sobre una recopilación de cuentos de Casciari, y a continuación, un desarrollo de la noche contada por un admirador de Hernán. Yo por mi parte, me despido hasta el próximo post. 

Cecilia

# Reunión del 30/11/18: Casciari



Como cierre del año hicimos el último encuentro de la Pizza Lectora en lo que va del 2018.

Las lecturas: creo que elegimos Casciari porque el fin de año nos encontraba a varios con cargas de diferente índole, y pienso que ayudaba a algunos que la "consigna" fuera leer cualquier relato del autor, y no sentir la presión de llevar leído una novela completa.

Mientras todavía la gente se iba preparando vimos [este video](https://www.youtube.com/watch?v=grSDwxCC9E0&t=6150s). Empecé poniendo un video de youtube porque el cuento sobre su infarto me pareció una buena presentación de lo que logra Casciari con su narrativa, y lo logra mejor aún cuando es él quien relata.  Yo tuve la suerte de escuchar cómo el tipo lo contaba en vivo. Como todavía no estábamos todes sentades (había gente solidaria que aún estaba ultimando la cena), me pareció adecuado porque es un cuento que empieza con un rodeo largo, como esperando a que la gente empiece a prestarle atención de a poco, mientras la historia crece y va tomando fuerza. Empieza bien light, _tranka_, y cuando menos lo esperás el tipo te agarra y te cambia el ritmo de la historia y es inevitable que te quedes sin aire, del golpe que te dio. Para cuando llegó al corazón de la historia ya estábamos todes infartades, porque nuestros corazones respiraban al ritmo de ese cuento, que nos había atrapado, a pesar de que cada tanto nos aflojaba un poco con sus chistes. El cuento es genial, pero escuchar los matices que él mismo da, Casciari en directo, es otro nivel completamente. 

Creo que por haber empezado con youtube se sugirió que siguiéramos con el cuento que se difundió como "Mi hija quiere conocer el sistema financiero", pero que nació llamándose **Papelitos**. A este cuento fue la primera vez que yo lo leía en su versión original (yo soy de los que conocieron la versión de youtube) y ambas versiones son buenas.

Luego de ese cuento, por pedido popular, pasamos a otro cuento largo: **10.6 segundos**. La maestría con la que Casciari retoma ese acontecimiento histórico (que no es poco importante para los argentinos) hace que la gente que no le gusta el fútbol (como yo, sin ir muy lejos) igual podamos saborear un poco de esa gloria del '86.

Para matizar pasamos a un cuento de otra índole: **La tenencia**. Aunque no lo considero uno de sus cuentos geniales, sí es representativo de lo que es la esencia de "El nuevo paraíso de los tontos", que es mi libro favorito de Hernán Casciari.

**El uno para el otro** es el siguiente, y sin respiro fuimos al último, porque estos cuentos, de alguna manera, muestran la contracara de las posibilidades de internet.

**Los justos** es el cuento con el que cierra "El nuevo paraíso de los tontos", y no es simplemente el último cuento sino que es el que aparece luego de la página que lleva como título "Epílogo". Si tuviera que recomendar a alguien un único cuento de Casciari a alguien, sería éste.

Y creo que no es un detalle menor que en el paraíso de los tontos, a pesar de todo, aún queden justos que nos salvan a todes.

LH

++++++++++++++++++\
\
\
\
Realizamos el tercer encuentro de la ahora llamada "La Pizza Lectora". Un
encuentro para hablar de libros mientras comemos pizza. En esta ocación
hablamos de 1984 de George Orwell, para seguir con el ciclo de futuros
distópicos.

La próxima reunión es el 20 de julio en Hackatory, Av. Freyre 3043 D4.

Esta es la [lista](https://hackatory.sandcats.io/shared/0NLPhefuJSMlpQk9AcRiP11eufVcK70WMxUPWZSEHa0)
votar por el próximas ediciones.
