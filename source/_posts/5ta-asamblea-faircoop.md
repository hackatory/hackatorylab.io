---
title: 5ta Asamblea Faircoop
date: 2019-06-19
tags:
  - faircoop
---

Después de un par de meses sin reuniones presenciales nos volvimos a juntar a
hablar para pensar como seguir con el movimiento, juntar fuerza, pensar
opciones.

Primero comentamos un poco los avances que fuimos haciendo estos meses, como la
reunión relámpago con Sebastian, de moneda Par y el proyecto que trajo
pancho" desde el estado con el cual podríamos vincularnos, la difusión
realizada en la flisol y otras actividades.

Después de eso, evaluamos distintas opciones para continuar, para poder seguir
difundiendo.

Una de las propuestas es llevar la criptodespensa como bandera a ferias
_hippie-chic_ dónde podemos encontrar usuarios interesados ideológicamente,
el problema que vimos es que en muchos casos estas personas no tienen tanta
necesidad de este tipo de monedas (están bancarizadas, tienen una situación
económica estable, etc.).

Otra opción que evaluamos es la de hablar con una organización como la
Asociación de Comerciantes de Aristóbulo del Valle para promocionar FairCoin
como un sistema de fidelización de clientes, la idea sería proveerles
billeteras de papel y capacitación, para que los comerciantes puedan aceptar
los fair como forma de pago.

También evaluamos la posibilidad de entregar créditos en faircoin sin interés,
para promover su uso en algunos mercados.

También se propuso dar una charla en la [Asociación Malatesta][1] para difundir
el faircoin y las criptomonedas.

Otra cuestión que se planteo, fue la idea de proponer el uso de FairCoin como
moneda cultural, para ser utilizada en recitales, obras de teatro, etc. El
problema que nos encontramos a este respecto es la mecánica de las entradas y
de las compras, ya que en muchos casos no tiene suficiente "agilidad", en este
tipo de ambientes.

Por último, se comentó de la idea de colaborar con la inversión para un
invernadero hidropónico. 

[1]: https://www.pausa.com.ar/2019/04/cultura-en-santa-fe-pero-con-acento-italiano/
