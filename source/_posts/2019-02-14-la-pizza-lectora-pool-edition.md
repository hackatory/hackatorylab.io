---
layout: blog
title: 'La Pizza Lectora: pool edition'
date: 2019-02-15T00:21:09.743Z
cover_index: /images/uploads/piza_lectora_cover.jpg
photos:
  - /images/uploads/piza_lectora_7.jpg
  - /images/uploads/piza_lectora_8.jpg
  - /images/uploads/piza_lectora_1.jpg
  - /images/uploads/piza_lectora_4.jpg
---
El último encuentro de LPL se desarrolló en el marco de las vacaciones de algunes integrantes. Esto permitió que las actividades del Club tomen lugar en una hermosa quinta en Colastiné Norte (nuestro agradecimiento a Raquel, que alquiló la quinta, nos dejó ir, nos prestó la parrilla y no nos echó cuando estuvo cansada. Grande.)

El libro seleccionado fue: El país de las Mujeres, de Gioconda Belli. GB es una escritora nicaragüense que se hizo conocida por escribir sobre temas tales como el cuerpo y la sexualidad de las mujeres, el rol de las mujeres en la sociedad y otra serie de temáticas feministas.

El país de las mujeres retrata cómo un partido político ficticio (PIE) llega al poder en un país ficticio (Faguas) y los eventos que se desarrollan a partir del atentado conta su vida que sufre la PresidentA Viviana Sansón.

Las opiniones fueron variadas y se tocaron temas como los prejuicios sobre el género, las penas a las violaciones, la sexualidad femenina como empoderamiento y si la que tenia marido era Rebeca o Ifi, porque los personajes secundarios eran medio que todos iguales (menos vos Juana de Arco, si estás leyendo esto, te amamos personaje de ficción).

Las Esbietopizzas se hicieron esperar (generando debate entre los mortales que no saben entender su grandeza sin condimento) pero finalmente llegaron. Después de: una bolsa de carbón, un palo de escoba, ramitas de todo tipo, 3 troncos (Verde, amarillo y rojo), 5 vasos de cerveza, una mamadera para Uma, 2 tetas para Aimé, 3 para Dante, el rescate de un gatito y la enorme colaboración de los compas Iván y Pablo. Y valieron la pena.

Tarde con perspectiva de género. Noche familiar.

Adjuntamos fotos del encuentro.

También dejamos el link para las [Esbietopizzas](https://www.youtube.com/watch?v=h_F1rWLTJmE&t=0s). Úsadlo bajo su responsabilidad, puede que el mundo de las pizzas ya no sea el mismo después de probarlas...
