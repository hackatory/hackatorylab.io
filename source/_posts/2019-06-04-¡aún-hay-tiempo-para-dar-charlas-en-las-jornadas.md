---
layout: blog
title: ¡Aún hay tiempo para dar charlas en las Jornadas!
date: 2019-06-04T13:50:01.098Z
cover_detail: /images/uploads/photo5156695576081967231.jpg
---
Las 2as jornadas de Género y Software Libre, el próximo jueves 4 y viernes 5 de julio de 2019, aún recibe propuestas de charlas y talleres.

Si te interesa, podés consultarnos al mail <mailto:tusl@unl.edu.ar>

Podés ver la página de las jornadas en <https://genysl.gitlab.io/>
