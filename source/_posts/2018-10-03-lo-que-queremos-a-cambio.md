---
title: Lo que pedimos a cambio
date: 2018-10-03T15:43:55.000Z
---
Una pregunta recurrente de los que se muestran interesados en venir es la pregunta de cuánto les va a costar el uso del espacio de coworking, nosotros ya dijimos que [no se trata de eso](/2016/12/30/2016-12-30-motivacion/), o nos interesa usar el Hackatory para hacer plata. Pero como la pregunta vuelve a aparecer una y otra vez,creo que hay que decir lo que _sí_ nos interesa.

Lo que intento hacer ahora es hacer explícito ese interés que nos mueve a juntarnos para trabajar en una misma oficina. Una de las cuestiones es compartir, no sólo el espacio, sino también las ideas y los conocimientos. Pero va más allá incluso, la idea es compartir también las herramientas, el tiempo, compartir charlas, compartir nuestros problemas y nuestros recursos.

De cierta manera, esta idea de compartir conocimiento no es nueva, está implícito en la idea de _cultura libre_, pero lo llevamos incluso más allá, llevándolo también al plano físico y a nuestro tiempo y esfuerzo.

Pero compartir no es gratuito (ni fácil), ya que va en contra de prácticas muy arraigadas, va de la mano con aprender a ofrecerse en lo cotidiano, aprender a convivir con otras personas. Así aprendemos a poner música, a hablar bajito sin molestar, a respetar la música que pone el otro, a preguntar opiniones y a decirlas.

Esas cuestiones, que parecen tan básicas, se vuelven muy complicadas cuando uno piensa en convivir sin que una persona escriba las reglas ella sola, sin que nadie tome el rol de jefe e indique qué está bien y qué esta mal. Escribir de a poco esas reglas, llegando a acuerdos comunes entre iguales, es lo que nos diferencia. Buscamos convivir sin decirle a nadie qué es lo que tiene que hacer.

Esta construcción, es a la vez nuestro aprendizaje, estamos entre todos haciendo el Hackatory y aprendiendo a hacerlo día a día. Proyectando nuestras ideas, que aislados no podemos hacer y juntándonos a aprender a trabajar colaborativamente.

Hacemos una reunión mensual, donde vemos cómo podemos continuar esta construcción.

Si venís, lo que pedimos a cambio es que compartas con nosotros, no sólo el lugar, sino también este camino de aprender a compartir.
